﻿using ExchangeRates;
using ExchangeRates.Enums;
using ExchangeRates.Structs;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Xml;

namespace Consumer
{
    class Program
    {
        static void Main(string[] args)
        {
            //Test1:
            //TestCsv();

            //Test2:
            TestXml();

            Console.Read();

        }

        public static void TestCsv()
        {
            Info info = new Info("test", "test2");

            RequestProperties req = CreateSampleRequestProp();

            req.returnType = ReturnTypes.ReturnDocumentType.CsvAsString;
            //req.returnType = ReturnTypes.ReturnDocumentType.Xml;

            (bool isAuthenticated, string responseMessage, object response, Type typeOfResponse) ret = info.Get(req);

            if (ret.isAuthenticated && ret.typeOfResponse == typeof(string))
            {
                string strCsv = ret.Item3 as string;
                Console.WriteLine(strCsv);
                File.AppendAllText("ER.csv", strCsv);
                
            }

            //Console.WriteLine(((XmlDocument)ret.Item3).InnerXml);



        }

        public static void TestXml()
        {
            Info info = new Info("test", "test2");

            RequestProperties req = CreateSampleRequestProp();

            req.returnType = ReturnTypes.ReturnDocumentType.XmlAsDocument;
           
            (bool isAuthenticated, string responseMessage, object response, Type typeOfResponse) ret = info.Get(req);

            if (ret.isAuthenticated && ret.typeOfResponse == typeof(XmlDocument))
            {
                XmlDocument xml = ret.Item3 as XmlDocument;
                Console.WriteLine(xml.InnerXml);
                File.AppendAllText("ER.xml", xml.InnerXml);
            }

         



        }

        //Change parameters of testing here
        public static RequestProperties CreateSampleRequestProp()
        {
            #region Sample data expected from the consumer app

            FilteringElement element = new FilteringElement("Kod", "=", Info.CurrencyCodes.USD.ToString(), "OR");
            FilteringElement element2 = new FilteringElement("Kod", "=", Info.CurrencyCodes.XDR.ToString(), null);

            SortingElement sortBy = new SortingElement("Kod", ClauseEnums.OrderDirectionWord.ASC.ToString());
            SortingElement sortBy2 = new SortingElement("ForexBuying", ClauseEnums.OrderDirectionWord.DESC.ToString());

            List<FilteringElement> elementsForFiltering = new List<FilteringElement>() { element, element2 };
            List<SortingElement> elementsForSorting = new List<SortingElement>() { sortBy, sortBy2 };

            ReadOnlyCollection<FilteringElement> filtering = new ReadOnlyCollection<FilteringElement>(elementsForFiltering);
            ReadOnlyCollection<SortingElement> sorting = new ReadOnlyCollection<SortingElement>(elementsForSorting);


            RequestProperties req = new RequestProperties();
            req.filterByElements = filtering;
            req.indexOfRatesXml = 1;

            req.sortByElements = sorting;
            req.sourceXmlURL = "http://www.tcmb.gov.tr/kurlar/today.xml";

            #endregion

            return req;

        }



    }
}
