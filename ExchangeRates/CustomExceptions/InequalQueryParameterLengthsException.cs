﻿using System;

namespace ExchangeRates.CustomExceptions
{
    [Serializable]
    public class InequalQueryParameterLengthsException : Exception
    {

        public InequalQueryParameterLengthsException()
        { }

        public InequalQueryParameterLengthsException(string message) : base(message)
        {

        }
    }
}
