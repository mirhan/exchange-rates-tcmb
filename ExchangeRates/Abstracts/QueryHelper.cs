﻿using ExchangeRates.Interfaces;
using System;
using System.Text;
using ExchangeRates.ClassExtensions;
using System.Collections.ObjectModel;

namespace ExchangeRates.Abstracts
{
    /// <summary>
    /// Prepares dataview query strings using given parameters
    /// </summary>
    public abstract class QueryHelper : IQueryHelper
    {

        #region  Properties

        private bool disposed = false;
        internal string FilteringQuery;
        internal string SortingQuery;

        #endregion

        #region Methods

        protected QueryHelper(ReadOnlyCollection<FilteringElement> filterByElements, ReadOnlyCollection<SortingElement> sortByElements)
        {
            FilteringQuery = CreateDataViewFilterQuery(filterByElements);
            SortingQuery = CreateDataViewSortQuery(sortByElements);

        }

        #endregion

        #region Methods
        public string CreateDataViewFilterQuery(ReadOnlyCollection<FilteringElement> filterByElements)
        {
           
                if (filterByElements == null || filterByElements.Count == 0)
                {
                    return string.Empty;
                }
                StringBuilder ret = new StringBuilder();

            for (int i = 0; i < filterByElements.Count; i++)
                {

                    ret.Append(filterByElements[i].ColumnName);
                    ret.Append(' ', 1);
                    ret.Append(filterByElements[i].Operand);
                    ret.Append(' ', 1);

                    var value = filterByElements[i].ColumnValue;



                    if (!value.IsNumeric()) ret.Append("'");

                    ret.Append(value);

                    if (!value.IsNumeric()) ret.Append("'");


                    if (i + 1 < filterByElements.Count)
                    {
                        ret.Append(' ', 1);
                        ret.Append(filterByElements[i].Conjunction);
                        ret.Append(' ', 1);


                    }

                }

                return ret.ToString();


            }


        public string CreateDataViewSortQuery(ReadOnlyCollection<SortingElement> sortByElements)
        {

            StringBuilder ret = new StringBuilder();

            if (sortByElements.Count == 0)
                        return ret.ToString();


            int counter = 0;

            while (counter < sortByElements.Count)
            {
                ret.Append(sortByElements[counter].ColumnName);
                ret.Append(' ', 1);
                ret.Append(sortByElements[counter].Direction);
                ret.Append(' ', 1);

                counter++;

                if (counter < sortByElements.Count)
                {
                    ret.Append(',', 1);
                }

            }


            return ret.ToString();


        }

        #endregion

        #region Disposing
        public void Dispose()
            {
                Dispose(true);
                GC.SuppressFinalize(this);
            }

            protected virtual void Dispose(bool disposing)
            {
                if (disposed) return;

                if (disposing)


                disposed = true;
            }

        #endregion
    }
}