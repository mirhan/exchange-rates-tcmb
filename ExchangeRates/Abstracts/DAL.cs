﻿using ExchangeRates.Interfaces;
using System;
using System.Xml;

namespace ExchangeRates.Abstracts
{
    /// <summary>
    /// Data Access Layer gets the raw data from the xml source.
    /// </summary>
    public abstract class DAL : IDAL
    {
        #region Properties

            /// <summary>
            /// Raw document from the source may contain multiple xml sources. 
            /// This number indicates the default index of the xml which serves the currency info. 
            /// </summary>
            public const byte DefaultIndexOfRatesXml = 1;

            public const string DefaultRawXmlUrl = "http://www.tcmb.gov.tr/kurlar/today.xml";

            public XmlDocument XDoc { get; set; }
            public XmlReader XmlRead { get; set; }
           

        #endregion

        #region Constructors
        /// <summary>
        /// Fills xDoc property with the given xml source when started.
        /// </summary>
        /// <param name="xmlUri"></param>
        protected DAL([System.Runtime.InteropServices.DefaultParameterValue(DefaultRawXmlUrl)] string xmlUri)
        {
            GetXmlReader(xmlUri);

        }
        #endregion       

        #region Actual Methods

        /// <summary>
        /// Fills XmlRead property with the given xml source.
        /// </summary>
        /// <param name="sourceXmlUrl"></param>        
        public void GetXmlReader([System.Runtime.InteropServices.DefaultParameterValue(DefaultRawXmlUrl)]string sourceXmlUrl)
        {
            if (disposed) throw new ObjectDisposedException(Strings.DAL, Strings.ExposedMessage);
            if (sourceXmlUrl is null) throw new ArgumentNullException(nameof(sourceXmlUrl));

            try
            {
                XmlRead = XmlReader.Create(sourceXmlUrl, new XmlReaderSettings());
            }
            catch (System.IO.FileNotFoundException ex)
            {
                throw ex;
            }
            catch (UriFormatException ex)
            {

                throw ex;

            }
            catch (Exception ex)
            {
                throw ex;
            }
            

        }

        #endregion

        #region Disposing

            bool disposed = false;
            public void Dispose()
            {
                Dispose(true);
                GC.SuppressFinalize(this);
            }
            protected virtual void Dispose(bool disposing)
            {
                if (disposed) return;

                if (disposing)
                {
                    if (XmlRead != null) XmlRead.Dispose();
                    if (XDoc != null) XDoc.RemoveAll();

                    disposed = true;
                }


            }

        #endregion

        #region Obsolete Methods
        /// <summary>
        /// Fills xDoc property with the given xml source.
        /// </summary>
        /// <param name="sourceXmlUrl">e.g.: http://www.tcmb.gov.tr/kurlar/today.xml </param>
        [Obsolete("This method will be deprecated soon. You could use GetXmlReader(string sourceXmlUrl) and XmlReader.ToToDataSet alternatively.")]
        public void GetXmlDocument([System.Runtime.InteropServices.DefaultParameterValue(DefaultRawXmlUrl)]string sourceXmlUrl)
        {
            if (disposed) throw new ObjectDisposedException(Strings.DAL, Strings.ExposedMessage);

            XDoc = new XmlDocument();
            XDoc.Load(sourceXmlUrl);

        }

        /// <summary>
        /// Fills xDoc property with the given xml source.
        /// </summary>
        /// <param name="sourceXml"></param>
        [Obsolete("This method will be deprecated soon. You could use GetXmlReader(string sourceXmlUrl) and XmlReader.ToToDataSet alternatively.")]
        public void GetXmlDocument(XmlReader sourceXml)
        {
            if (disposed) throw new ObjectDisposedException(Strings.DAL, Strings.ExposedMessage);

            XDoc = new XmlDocument();
            XDoc.Load(sourceXml);

        }

        #endregion
    }
}
