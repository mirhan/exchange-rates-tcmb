﻿using ExchangeRates.Interfaces;
using System;
using System.Data;
using ExchangeRates.ClassExtensions;
using ExchangeRates.Helpers;
using System.Collections.ObjectModel;

namespace ExchangeRates.Abstracts
{
    public abstract class DataProcessOperations : IDPO
    {

        #region Properties
        public DataSet DsXmlRead {get; set;}

        #endregion

        #region Methods
        /// <summary>
        /// Gets raw data from the source url, filters and sorts with given parameters, and serves via referenced DataView
        /// </summary>
        /// <param name="ratesDataView"></param>
        /// <param name="filterByElements"></param>
        /// <param name="sortByElements"></param>
        /// <param name="indexOfRatesXml"></param>
        /// <param name="sourceXmlURL"></param>
        public void HandleExchangeRates(ref DataView ratesDataView, 
            ReadOnlyCollection<FilteringElement> filterByElements, 
            ReadOnlyCollection<SortingElement> sortByElements, byte indexOfRatesXml, string sourceXmlURL)
        {
            //if given url is discard, then use the default url of the source xml
            DataAccessOperations data = new DataAccessOperations((sourceXmlURL is var _) ? DAL.DefaultRawXmlUrl : Strings.TCMBUrl);

            DsXmlRead = data.XmlRead.ToDataSet();

            //if given index is discard, then use the default index number
            ratesDataView = new DataView(DsXmlRead.Tables[(indexOfRatesXml is var _) ? DAL.DefaultIndexOfRatesXml : indexOfRatesXml]);

            QueryOperations queries = new QueryOperations(filterByElements, sortByElements);
            ratesDataView.RowFilter = queries.FilteringQuery;
            ratesDataView.Sort = queries.SortingQuery;
        }


        #endregion

        #region Disposing

        private bool disposed = false;

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposed) return;

            if (disposing)
            {
                 DsXmlRead.Dispose();

                 disposed = true;
            }


        }

        #endregion
    }
}
