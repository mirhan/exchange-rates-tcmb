# Exchange Rates

This class library project serves exchange rates data as asked by the consumer, using a given XML source.

## Getting Started

Main servant method is : new Info(...).Get(...);

## Authentication

This app is planned serving just to registered users with a daily usage limit because of performance and security drawbacks.

Authentication is noted to be done online with a username and password within usage limits.

During beta phase, authentication is done by checking the last character of the password given.
If it is a number > 0 and < 9, the user is authenticated.

### Prerequisites

The default XML source is of The Central Bank of the Republic of Turkey
at http://www.tcmb.gov.tr/kurlar/today.xml.

This source provides the expected data at its second node.

If the default source is going to be used, the url and the schema must be checked if they are changed or not.
Titles used in the XML must also be known as they may be used for sorting and filtering. 
These are done by their titles instead of the index numbers, considering their order may be changed by the source in an unknown future.

Since the receiver methods are using dynamic parameters, 
the project is ready for possible minor changes at the source, like the change of URL, or order of the source node.

The URL and node index may be given by the consumer.

If another XML source is going to be used, these data must also be known before using:

- The URL of the XML source.
- Index of the node which serves the exchange rates.

Schemas are also created dynamically so they are not needed to be attached to the project.

### Installing

Output dll should be referenced in a project that is intended to be used by.

ValueTuple may need to be installed: Install-Package "System.ValueTuple"

## Running the tests

Unit Testing are covering core parts of the implementation. Not all the units are being tested yet.

Live unit testing is also possible to be used.

### End to end tests

You may follow the End-to-end regions at InfoTests and AuthenticationTests.

You may also use the console application named Consumer in the solution.

As the Get method being a unit of the Business Layer, it is also tested by the Unit Testing project. 

## Adding plugins

Please follow:

https://code.msdn.microsoft.com/windowsdesktop/Creating-a-simple-plugin-b6174b62

## Adding new export methods

Assuming you are going to export to X:

1- Create your XOperations class in BLL folder.
2- It should include a method converting from DataView or DataSet to X.
3- Add related items to ReturnTypes enum.
4- Call your converter method in the switch-caseGet method of Info.cs


## Security

The DLL is intentionally not signed with a strong name relying on a common modern security perspective as referenced below:

https://www.pedrolamas.com/2016/03/01/still-strong-naming-your-assemblies-you-do-know-its-2016-right/

## Design Pattern

Dispose Design Pattern is used.

Although there are not many unmanaged resources, 
considering this app may be used by many users frequently, and this may spend system resources much,
Dispose Design pattern is used in the design of this api.

This pattern is also one of two patterns that Microsoft puts forward for .NET Framework APIs.
(https://docs.microsoft.com/tr-tr/dotnet/standard/design-guidelines/common-design-patterns)

### Guidelines supporting the design pattern

Besides the coding design of this design pattern, guidelines with the same aim by Microsoft are also followed:

"Too many abstractions in a framework also negatively affect usability of the framework."

"Therefore, base classes should be used only if they provide significant value to the users of the framework."

"DO NOT use ArrayList or List<T> in public APIs.
These types are data structures designed to be used in internal implementation, not in public APIs."

"DO NOT use Hashtable or Dictionary<TKey,TValue> in public APIs."

"DO use ReadOnlyCollection<T>, a subclass of ReadOnlyCollection<T>, 
or in rare cases IEnumerable<T> for properties or return values representing read-only collections.
In general, prefer ReadOnlyCollection<T>"

"DO NOT return null values from collection properties or from methods returning collections. Return an empty collection or an empty array instead."

### Managed Resources 

Managed resources are also freed as advised. (http://www.informit.com/articles/article.aspx?p=2756468&seqNum=7)

## Built With

* [Visual Studio](https://www.visualstudio.com) - 2017 / v15.1
* [.Net Framework](https://www.microsoft.com/en-us/download/details.aspx?id=49982) - v4.6.1

## Versioning

We use [SemVer](http://semver.org/) for versioning. 

## Authors

Mirhan Karadeniz (mirhankaradeniz@g-m-a-i-l.com)

## License

This project is licensed under the Personal Use License.

