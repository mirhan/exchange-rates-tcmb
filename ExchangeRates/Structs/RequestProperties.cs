﻿using System.Collections.ObjectModel;
using static ExchangeRates.Enums.ReturnTypes;

namespace ExchangeRates.Structs
{
    /// <summary>
    /// Supplied by the consumer, this data is used to prepare the final document.
    /// </summary>
    public struct RequestProperties
    {
        /// <summary>
        /// URL of the source XML which serves the exchange rates. _ if unknown.
        /// </summary>        
        public string sourceXmlURL { get; set; }
        /// <summary>
        /// Index of the XML schema which serves the exchange rates. _ if unknown.
        /// </summary>
        public byte indexOfRatesXml { get; set; }
        public ReadOnlyCollection<FilteringElement> filterByElements { get; set; }
        public ReadOnlyCollection<SortingElement> sortByElements { get; set; }

        /// <summary>
        /// The type that the consumer asks the data to be in.
        /// </summary>
        public ReturnDocumentType returnType { get; set; }
    }
}
