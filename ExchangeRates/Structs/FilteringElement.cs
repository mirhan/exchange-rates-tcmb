﻿namespace ExchangeRates
{
    /// <summary>
    /// Element to be used by preparing DataView query string for filtering the data.
    /// </summary>
    public struct FilteringElement
    {

        public FilteringElement(string columnName, string operand, string columnValue, string conjunction)
        {

            ColumnName = columnName;
            Operand = operand;
            ColumnValue = columnValue;
            Conjunction = conjunction;

        }

        public string ColumnName { get; set; }

        /// <summary>
        /// e.g. =, >=
        /// </summary>
        public string Operand { get; set; }

        /// <summary>
        /// Convert numeric values to string as well.
        /// </summary>
        public string ColumnValue { get; set; }

        /// <summary>
        /// AND, OR
        /// </summary>
        public string Conjunction { get; set; }

    }
}
