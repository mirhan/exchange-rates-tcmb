﻿namespace ExchangeRates
{
    /// <summary>
    /// Element to be used by preparing DataView query string for sorting the data.
    /// </summary>
    public struct SortingElement
    {
        public SortingElement(string columnName, string direction)
        {
            ColumnName = columnName;
            Direction = direction;

        }
        public string ColumnName { get; set; }

        /// <summary>
        /// ASC or DESC
        /// </summary>
        public string Direction { get; set; }
    }
}
