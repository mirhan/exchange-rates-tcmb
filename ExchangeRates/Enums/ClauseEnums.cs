﻿namespace ExchangeRates
{
    public struct ClauseEnums
    {

        public enum OrderDirectionWord
        {
            ASC,
            DESC
        }

        public enum Conjunction
        {
            AND,
            OR

        }

    }
}
