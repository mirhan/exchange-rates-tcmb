﻿namespace ExchangeRates.Enums
{
    public struct ReturnTypes
    {

        public enum ReturnDocumentType
        {
            XmlAsDocument  = 0,
            CsvAsByteArray  = 1,
            CsvAsString = 2

        }
    }
}
