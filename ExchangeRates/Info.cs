﻿using ExchangeRates.Attributes;
using ExchangeRates.BLL;
using ExchangeRates.Helpers;
using ExchangeRates.Interfaces;
using ExchangeRates.Structs;
using System;
using System.ComponentModel;
using System.Data;
using System.Xml;
using static ExchangeRates.Enums.ReturnTypes;

namespace ExchangeRates
{

    /// <summary>
    /// This is the main point that a consumer is supposed to be served. " +
    /// "Simply, new Info(...).Get(...)  returns the exchange rates data.
    /// </summary>
    [StartPoint]
    public class Info : IInfo<Info.CurrencyCodes>
    {

        #region Business Properties
        
        ///To be filled by the constructor, holds the (Is User Authenticated & Response Message) info.
        public static (bool IsAuthenticated, string ReponseMessage) AuthenticationMessage { get; set; }    
        
        public enum CurrencyCodes
        {
            USD,
            AUD,
            DKK,
            EUR,
            GBP,
            CHF,
            SEK,
            CAD,
            KWD,
            NOK,
            SAR,
            JPY,
            BGN,
            RON,
            RUB,
            IRR,
            CNY,
            PKR,
            XDR

        }

        public CurrencyCodes Codes { get; set; }

        public (string XmlText, string Schema) XmlTextWithSchema;

        public string UserName { get; set; }

        [PasswordPropertyText(true)]
        public string Password { get; set; }

        public DataProcessor dataPro;

        #endregion

        #region Constructors

        /// <summary>
        /// Authenticates the user as soon as started.
        /// </summary>
        /// <param name="userName"></param>
        /// <param name="password"></param>
        public Info(string userName, string password)
        {
            UserName = userName;
            Password = password;

            AuthenticationMessage = new Authentication().AuthenticateUser(userName, password);

         

        }

        #endregion

        #region Methods

        /// <summary>
        /// Serves the exchange rates sorted and filtered by given parameters in asked file format with authentication info
        /// </summary>
        /// <param name="req">_ for unknown parameters.</param>
        /// <returns>Is Authenticated, Response Message, File Object, File Type.</returns>
        [StartPoint]
        public (bool IsAuthenticated, string ReponseMessage, object Document, Type TypeOfTheDocument) 
            Get(RequestProperties req)
        {
            
            if (AuthenticationMessage.IsAuthenticated == false)
            {
                return (false, AuthenticationMessage.Item2, null, typeof(Object));
            }

            DataView dv = new DataView();
            dataPro = new DataProcessor();
            dataPro.HandleExchangeRates(ref dv, req.filterByElements, req.sortByElements, req.indexOfRatesXml, req.sourceXmlURL);

            //This object is going to hold the returning document of which the type is given by the user.
            object ret;

            switch (req.returnType)
            {
                case ReturnDocumentType.XmlAsDocument:
                    XmlTextWithSchema = new XmlOperations().GetXMLTextWithSchema(dv);
                    ret = new XmlOperations().MergeXmlWithSchema(XmlTextWithSchema.XmlText, XmlTextWithSchema.Schema);
                    //dv.Table.WriteXml("ER.xml");
                
                    return (true, AuthenticationMessage.Item2, ret, typeof(XmlDocument));
                case ReturnDocumentType.CsvAsByteArray:
                    ret = new CsvOperations().ExportToCsvAsByteArray(dv.Table);
            
                    return (true, AuthenticationMessage.Item2, ret, typeof(byte[]));

                case ReturnDocumentType.CsvAsString:
                    ret = new CsvOperations().ExportToCSVAsString(dv.Table);
     
                    return (true, AuthenticationMessage.Item2, ret, typeof(string));


            }

            //Although this line is not reachable, it is needed for method writing guides.
            return (false, string.Empty, null, typeof(Object));

            
        }

        #endregion

        #region Disposing

        private bool disposed = false;

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposed) return;

            if (disposing)
            {
                AuthenticationMessage = (false, null);
                XmlTextWithSchema = (null, null);
                dataPro.Dispose();

               disposed = true;
            }


        }

        #endregion  

    }
}
