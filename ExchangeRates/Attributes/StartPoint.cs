﻿using System;

namespace ExchangeRates.Attributes
{
    /// <summary>
    /// Indicating the showcase that consumers supposed to use for the main purpose of the assembly.
    /// </summary>
    public class StartPoint : Attribute
    {
        
    }
}
