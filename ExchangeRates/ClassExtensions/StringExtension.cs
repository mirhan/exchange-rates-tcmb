﻿
namespace ExchangeRates.ClassExtensions
{
    public static class StringExtension
    {

        public static string LastCharacter(this string value)
        {
            
            if (string.IsNullOrEmpty(value))
            {
                return string.Empty;
            }

            if (value.Length == 1)
            {
                return value;
            }

            return value.Substring(value.Length - 1, 1);

        }

        public static bool IsNumeric(this string value)
        {

            return int.TryParse(value, out int n);

        }

        public static byte AsByte(this string value)
        {
            
            byte.TryParse(value, out byte ret);

            return ret;


        }

    }
}
