﻿using System.Data;
using System.Xml;

namespace ExchangeRates.ClassExtensions
{
    public static class XmlReaderExtension
    {

        public static DataSet ToDataSet(this XmlReader xReader )
        {
            DataSet ret = new DataSet();
            ret.ReadXml(xReader);

        
            return ret;
        }
    
    }
}
