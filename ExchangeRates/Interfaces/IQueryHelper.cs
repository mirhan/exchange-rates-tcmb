﻿using System;
using System.Runtime.CompilerServices;
using System.Collections.ObjectModel;

[assembly: InternalsVisibleTo("TestExchangeRates")]
namespace ExchangeRates.Interfaces
{
    interface IQueryHelper : IDisposable
    {

        string CreateDataViewFilterQuery(ReadOnlyCollection<FilteringElement> filterByElements);

        string CreateDataViewSortQuery(ReadOnlyCollection<SortingElement> sortByElements);

    }
}
