﻿namespace PluginContracts
{
    /// <summary>
    /// To provide a plugin, you have to create a new project and add a reference to PluginContracts. 
    /// Then implement IPlugin.    
    /// After th project is finished, place the output dll into the Plugins folder.
    /// Find the plugins as follows:
    /// _Plugins = new Dictionary<string, IPlugin>(); 
    /// ICollection<IPlugin> plugins = PluginLoader.LoadPlugins("Plugins"); 
    /// foreach(var item in plugins) 
    /// { 
    /// _Plugins.Add(item.Name, item); 
    /// 
    /// Then run the plugin as follows:
    /// if(_Plugins.ContainsKey(key)) 
    /// { 
    ///  IPlugin plugin = _Plugins[key];
    ///  plugin.Do(); 
    /// }
    /// Reference: https://code.msdn.microsoft.com/windowsdesktop/Creating-a-simple-plugin-b6174b62
    /// </summary>
    public interface IPlugin
    {
        string Name { get; }
        void Do();
    }
}
