﻿using System;
using System.Runtime.CompilerServices;
using System.Data;
using System.Collections.ObjectModel;

[assembly: InternalsVisibleTo("TestExchangeRates")]
namespace ExchangeRates.Interfaces
{
    /// <summary>
    /// Interface of Data Process Operations.
    /// </summary>
    interface IDPO : IDisposable
    {
        void HandleExchangeRates(ref DataView ratesDataView, 
            ReadOnlyCollection<FilteringElement> filterByElements, 
            ReadOnlyCollection<SortingElement> sortByElements, 
            byte indexOfRatesXml, 
            string sourceXmlURL);
    }
}
