﻿using System;
using System.Data;

namespace ExchangeRates.Interfaces
{
    interface ICsvOperations : IDisposable
    {

        byte[] ExportToCsvAsByteArray(DataTable dataTable);
    }
}
