﻿using System;
using System.Data;
using System.Xml;
using System.Runtime.CompilerServices;

[assembly: InternalsVisibleTo("TestExchangeRates")]
namespace ExchangeRates.Interfaces
{
    interface IXmlOperations : IDisposable
    {

        (string Xml, string Schema) GetXMLTextWithSchema(DataView dv);


        XmlDocument MergeXmlWithSchema(string xml, string xmlSchema);

    }
}
