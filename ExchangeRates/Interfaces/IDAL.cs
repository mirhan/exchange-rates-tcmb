﻿using System;
using System.Runtime.CompilerServices;
using System.Xml;

[assembly: InternalsVisibleTo("TestExchangeRates")]
namespace ExchangeRates.Interfaces
{
    interface IDAL : IDisposable
    {       

        XmlDocument XDoc { get; set; }
        XmlReader XmlRead { get; set; }

        void GetXmlDocument(string sourceXmlUrl);

        void GetXmlDocument(XmlReader sourceXml);

        void GetXmlReader(string sourceXmlUrl);

    }
}
