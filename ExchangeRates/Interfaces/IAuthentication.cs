﻿using System;
using System.Runtime.CompilerServices;

[assembly: InternalsVisibleTo("TestExchangeRates")]
namespace ExchangeRates.Interfaces
{
    interface IAuthentication : IDisposable
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="username"></param>
        /// <param name="password"></param>
        /// <returns>Returns if user is authenticated with an info message.</returns>
        (bool,string) AuthenticateUser(string username, string password);

      

    }
}
