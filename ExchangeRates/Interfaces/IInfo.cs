﻿using ExchangeRates.Structs;
using System;

namespace ExchangeRates.Interfaces
{
    interface IInfo<T> : IDisposable
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="req"></param>
        /// <returns>Is User Athenticated, Response Message, Response Object, Type Of Response Object</returns>
        (bool IsAuthenticated, string ReponseMessage, object Document, Type TypeOfTheDocument) Get(RequestProperties req);

        //(bool, string, byte[]) GetCsv(List<FilteringElement> filterByElements, List<SortingElement> sortByElements, string userName, string password, int indexOfRatesXml);


        //This is for making enum possible to be used by methods as parameters
        T Codes { get; set; }


    }
}
