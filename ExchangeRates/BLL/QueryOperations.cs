﻿using System.Runtime.CompilerServices;
using ExchangeRates.Abstracts;
using System.Collections.ObjectModel;

[assembly: InternalsVisibleTo("TestExchangeRates")]
namespace ExchangeRates.Helpers
{
    public class QueryOperations : QueryHelper
    {
        public QueryOperations(ReadOnlyCollection<FilteringElement> filterByElements, ReadOnlyCollection<SortingElement> sortByElements) : base(filterByElements, sortByElements)
        {
        }
    }
}
