﻿using ExchangeRates.Interfaces;
using System;
using ExchangeRates.ClassExtensions;
using System.ComponentModel;
using System.Runtime.CompilerServices;

[assembly: InternalsVisibleTo("TestExchangeRates")]
namespace ExchangeRates
{
    [Description("This is a dummy authentication class that should ideally authenticate online." +
       "The class is supposing that the user has a daily usage limit of 10." +
        "If the password is ending with a number, and if the number is smaller than 9, the user is authenticated.")]
    internal class Authentication : IAuthentication
    {
        #region Properties
        //TODO: Get default value online.
        private const int DefaultMaximumDailyUsageRight = 10;

        #endregion

        #region Methods
        //TODO: Online authentication.
        public (bool, string) AuthenticateUser(string username, String password)
            {
                //Dummy Code:

                byte usedTimes = password.LastCharacter().AsByte();

                if (usedTimes <= 0)
                {
                    return (false, Strings.NotAuthenticated);
                }

                byte left = CountOfUsageRightLeft(usedTimes);
                string message = GetUsageMessage(left);

                if (left > 0)
                {
                   return (true, message);
                }
                else 
                {
                    return (false, message);
                }
                  
                
            }

        internal string GetUsageMessage(int left)
        {
                switch (left)
                {
                    case 0:
                        return Strings.Exceeded;
                    case 1:
                        return Strings.OneMoreRight;
                    default:
                        return $"You may ask for {left} times more.";
                }
            
        }

        /// <summary>
        ///  Helper for dummy code
        /// </summary>
        /// <param name="usedTimes"></param>
        /// <returns></returns>
        internal byte CountOfUsageRightLeft(byte usedTimes)
        {
           
            return (byte)(DefaultMaximumDailyUsageRight - usedTimes - 1); 
        }

        #endregion

        #region Disposing

        private bool disposed;
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposed) return;

            if (disposing)
            {
                //Intentionally left blank for future need.

                disposed = true;
            }


        }

        #endregion


    }

}
