﻿using ExchangeRates.Interfaces;
using System;
using System.Xml;
using System.Data;
using System.Runtime.CompilerServices;

[assembly: InternalsVisibleTo("TestExchangeRates")]

namespace ExchangeRates.Helpers
{
    internal class XmlOperations : IXmlOperations
    {
        internal XmlDocument Ret;

        #region Methods
        public XmlDocument MergeXmlWithSchema(string xml, string xmlSchema)
        {
            xml = xml.Replace("<NewDataSet>", string.Empty);
            xmlSchema = xmlSchema.Replace("<xs:schema", "<NewDataSet>" + "<xs:schema");

            Ret = new XmlDocument();
            Ret.LoadXml(xmlSchema + xml);

            return Ret;
        }

        public (string Xml, string Schema) GetXMLTextWithSchema(DataView dv)
        {
            DataTable dt = dv.ToTable();
            DataSet retDs = new DataSet();

            retDs.Tables.Add(dt);

            return (retDs.GetXml(), retDs.GetXmlSchema());



        }

        #endregion

        #region Disposing

        bool disposed = false;
            public void Dispose()
            {
                Dispose(true);
                GC.SuppressFinalize(this);
            }

            protected virtual void Dispose(bool disposing)
            {
                if (disposed) return;

                if (disposing)

                    Ret = null;
               
                    disposed = true;
            }

        #endregion


    }
}
