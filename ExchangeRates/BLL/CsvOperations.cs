﻿using System;
using System.Data;
using System.Text;
using System.Runtime.CompilerServices;
using ExchangeRates.Interfaces;

[assembly: InternalsVisibleTo("TestExchangeRates")]
namespace ExchangeRates.Helpers
{
    internal class CsvOperations : ICsvOperations
    {
        #region Methods
        public byte[] ExportToCsvAsByteArray(DataTable dataTable)
        {
            if (dataTable is null)
            {
                return new byte[0];
            }
            

            return Encoding.UTF8.GetBytes(ExportToCSVAsString(dataTable));
           
        }
        
        public string ExportToCSVAsString(DataTable dataTable)
        {
            if (dataTable is null)
            {
                return string.Empty;
            }

            StringBuilder sb = new StringBuilder();
            var intClmn = dataTable.Columns.Count;

         
            for (i = 0; i <= intClmn - 1; i += i + 1)
            {
                sb.Append("\"" + dataTable.Columns[i].ColumnName + "\"");
                sb.Append(i == intClmn - 1 ? " " : ",");
            }
            sb.Append(Environment.NewLine);


            foreach (DataRow row in dataTable.Rows)
            {
                var ir = 0;
                for (ir = 0; ir <= intClmn - 1; ir += ir + 1)
                {
                    sb.Append("\"" + row[ir].ToString().Replace("\"", "\"\"") + "\"");
                    sb.Append(ir == intClmn - 1 ? " " : ",");
                }
                sb.Append(Environment.NewLine);
            }

          
            return sb.ToString();

        }

        #endregion

        #region Disposing

        bool disposed = false;
        private int i;

        public void Dispose()
            {
                Dispose(true);
                GC.SuppressFinalize(this);
            }

            protected virtual void Dispose(bool disposing)
            {
                if (disposed) return;

                if (disposing)


                    disposed = true;
            }

        #endregion

      

    }
}
