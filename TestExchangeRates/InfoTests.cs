﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using ExchangeRates;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ExchangeRates;
using System.Collections.ObjectModel;
using ExchangeRates.Enums;
using System.Data;
using System.Xml;
using ExchangeRates.BLL;
using ExchangeRates.ClassExtensions;
using ExchangeRates.Helpers;
using ExchangeRates.Structs;

namespace ExchangeRates.Tests
{
    [TestClass]
    public class InfoTests
    {

        #region End-to-End testing

        [Description("This method prepares the properties to be used by end-to-end testing methods GetXmlTest, GetCsvTest. " +
                     "You may change these properties the way you desire to test them.")]
        public RequestProperties GetSampleRequestProperties()
        {

            FilteringElement element = new FilteringElement("Kod", "=", Info.CurrencyCodes.USD.ToString(), "OR");
            FilteringElement element2 = new FilteringElement("Kod", "=", Info.CurrencyCodes.XDR.ToString(), null);

            SortingElement sortBy = new SortingElement("Kod", ClauseEnums.OrderDirectionWord.ASC.ToString());
            SortingElement sortBy2 = new SortingElement("ForexBuying", ClauseEnums.OrderDirectionWord.DESC.ToString());

            List<FilteringElement> elementsForFiltering = new List<FilteringElement>() { element, element2 };
            List<SortingElement> elementsForSorting = new List<SortingElement>() { sortBy, sortBy2 };

            ReadOnlyCollection<FilteringElement> filtering = new ReadOnlyCollection<FilteringElement>(elementsForFiltering);
            ReadOnlyCollection<SortingElement> sorting = new ReadOnlyCollection<SortingElement>(elementsForSorting);

            RequestProperties req = new RequestProperties();
            req.filterByElements = filtering;
            req.indexOfRatesXml = 1;
            req.returnType = ReturnTypes.ReturnDocumentType.XmlAsDocument;
            req.sortByElements = sorting;
            req.sourceXmlURL = Strings.TCMBUrl;

            return req;
        }

        [TestMethod()]
        public void GetXmlTest()
        {
            RequestProperties req = GetSampleRequestProperties();
            string sourceXmlURL = req.sourceXmlURL;
            int indexOfRatesXml = req.indexOfRatesXml;

            //if given url is discard, then use the default url of the source xml
            DataAccessOperations data = new DataAccessOperations((sourceXmlURL is var _) ? Abstracts.DAL.DefaultRawXmlUrl : Strings.TCMBUrl);
            DataSet ds = data.XmlRead.ToDataSet();

            ////if given index is discard, then use the default index number
            DataView ratesDataView = new DataView(ds.Tables[(indexOfRatesXml is var _) ? Abstracts.DAL.DefaultIndexOfRatesXml : indexOfRatesXml]);


            QueryOperations queries = new QueryOperations(req.filterByElements, req.sortByElements);
            ratesDataView.RowFilter = queries.FilteringQuery;
            ratesDataView.Sort = queries.SortingQuery;


            (string XmlText, string Schema) XmlTextWithSchema = new XmlOperations().GetXMLTextWithSchema(ratesDataView);

            XmlDocument ret = new XmlOperations().MergeXmlWithSchema(XmlTextWithSchema.XmlText, XmlTextWithSchema.Schema);

            Assert.IsInstanceOfType(ret, typeof(XmlDocument));

        }

        [TestMethod()]
        public void GetCsvTest()
        {
            RequestProperties req = GetSampleRequestProperties();
            string sourceXmlURL = req.sourceXmlURL;
            int indexOfRatesXml = req.indexOfRatesXml;

            //if given url is discard, then use the default url of the source xml
            DataAccessOperations data = new DataAccessOperations((sourceXmlURL is var _) ? Abstracts.DAL.DefaultRawXmlUrl : Strings.TCMBUrl);
            DataSet ds = data.XmlRead.ToDataSet();

            ////if given index is discard, then use the default index number
            DataView ratesDataView = new DataView(ds.Tables[(indexOfRatesXml is var _) ? Abstracts.DAL.DefaultIndexOfRatesXml : indexOfRatesXml]);


            QueryOperations queries = new QueryOperations(req.filterByElements, req.sortByElements);
            ratesDataView.RowFilter = queries.FilteringQuery;
            ratesDataView.Sort = queries.SortingQuery;

            object ret = new CsvOperations().ExportToCsvAsByteArray(ratesDataView.Table);

            Assert.IsInstanceOfType(ret, typeof(byte[]));

        }

        #endregion 


        [TestMethod()]
        public void GetTest_Xml_Auth()
        {
            RequestProperties req = GetSampleRequestProperties();
            (bool, string, object, Type) ret = new Info("test","test2").Get(req);


            Assert.AreEqual(true, ret.Item1);
        }

        [TestMethod()]
        public void GetTest_Xml_Msg()
        {
            RequestProperties req = GetSampleRequestProperties();
            (bool, string, object, Type) ret = new Info("test", "test2").Get(req);

            Assert.AreEqual("You may ask for 7 times more.", ret.Item2);
        }

        [TestMethod()]
        public void GetTest_DataView()
        {
            Info info = new Info("test", "test2");

            RequestProperties req = GetSampleRequestProperties();

            DataView dv = new DataView();
            DataProcessor dataPro = new DataProcessor();
            dataPro.HandleExchangeRates(ref dv, req.filterByElements, req.sortByElements, req.indexOfRatesXml, req.sourceXmlURL);

            Assert.IsNotNull(dv);
         

            //Assert.IsNotInstanceOfType(dv, typeof(DataView));
        }

     

        [TestMethod()]
        public void GetDefaultXmlAsDataSetTest()
        {
            RequestProperties req = GetSampleRequestProperties();
            string sourceXmlURL = req.sourceXmlURL;
            int indexOfRatesXml = req.indexOfRatesXml;

            //if given url is discard, then use the default url of the source xml
            DataAccessOperations data = new DataAccessOperations((sourceXmlURL is var _) ? Abstracts.DAL.DefaultRawXmlUrl : Strings.TCMBUrl);
            DataSet ds = data.XmlRead.ToDataSet();

            Assert.IsNotNull(ds.Tables[indexOfRatesXml]);
            
        }

       



      


    }
}