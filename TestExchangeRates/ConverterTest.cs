﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using ExchangeRates;
using System.Data;
using ExchangeRates.ClassExtensions;
using ExchangeRates.Helpers;

namespace TestExchangeRates
{
    [TestClass]
    public class ConverterTest
    {

        public void SetDataTableForTest (ref DataTable dt)
        {
            DataAccessOperations data = new DataAccessOperations("http://www.tcmb.gov.tr/kurlar/today.xml");
         
            DataSet ds = data.XmlRead.ToDataSet();

            dt = ds.Tables[1];

        }

        /// <summary>
        /// Test Csv Conversion Under Normal Circumstances
        /// </summary>
        [TestMethod]
        public void TestCsvUNC()
        {
            DataTable dt = new DataTable();
            SetDataTableForTest(ref dt);

            byte[] ret = new CsvOperations().ExportToCsvAsByteArray(dt);

            Assert.IsNotNull(ret);

        }


        /// <summary>
        /// Test Csv Conversion when passed null reference
        /// </summary>
        [TestMethod]
        public void TestCsvWithNullSource()
        {
            //DataTable dt = new DataTable();
            //SetDataTableForTest(ref dt);

            byte[] ret = new CsvOperations().ExportToCsvAsByteArray(null);

            Assert.AreEqual(ret.Length, 0);
        }


        [TestMethod]
        public void TestXmlRegular()
        {

            //DAL dal = new DAL();
            //DataSet ds =  dal.XmlRead("http://www.tcmb.gov.tr/kurlar/today.xml");

            //Assert.IsNotNull(ds);


        }


       

    }
}
