﻿using System;
using System.Configuration;
using ExchangeRates;
using ExchangeRates.ClassExtensions;
using ExchangeRates.Enums;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace TestExchangeRates
{
    [TestClass]
    public class AuthenticationTest
    {
       
        #region End To End Testing

        [TestMethod]
        public void AuthenticateUser_UsingInfoClass()
        {

            //Info info = new Info("test", "test2");
            Info.AuthenticationMessage = new Authentication().AuthenticateUser("test", "test2");

            Assert.AreEqual(true, Info.AuthenticationMessage.IsAuthenticated);
        }
        
        [TestMethod]
        public void AuthenticateUser_Ending9()
        {
            //Dummy Code:
            Authentication auth = new Authentication();
            (bool, string) ret = auth.AuthenticateUser("test", "test9");
            Assert.AreEqual((false, Strings.Exceeded), ret);

        }

        [TestMethod]
        public void AuthenticateUser_Ending0()
        {
            //Dummy Code:
            Authentication auth = new Authentication();
            (bool, string) ret = auth.AuthenticateUser("test", "test0");
            Assert.AreEqual((false, Strings.NotAuthenticated), ret);

        }
        [TestMethod]
        public void AuthenticateUser_EndingStr()
        {
            //Dummy Code:
            Authentication auth = new Authentication();
            (bool, string) ret = auth.AuthenticateUser("test", "test");
            Assert.AreEqual((false, Strings.NotAuthenticated), ret);

        }

        [TestMethod]
        public void AuthenticateUser_Ending2()
        {
            //Dummy Code:
            Authentication auth = new Authentication();
            (bool, string) ret = auth.AuthenticateUser("test", "test2");
            Assert.AreEqual((true, $"You may ask for 7 times more."), ret);

        }

        [TestMethod]
        public void AuthenticateUser_EndingNull()
        {
            //Dummy Code:
            Authentication auth = new Authentication();
            (bool, string) ret = auth.AuthenticateUser("test", null);
            Assert.AreEqual((false, Strings.NotAuthenticated), ret);

        }

        #endregion

        [TestMethod]
        public void CountOfUsageRightLeft()
        {

            byte ret = new Authentication().CountOfUsageRightLeft(9);
            byte expected = 0;

            Assert.AreEqual(expected, ret);
        }

        [TestMethod]
        public void GetUsageMessageTest_Zero()
        {
            string ret = new Authentication().GetUsageMessage(0);
            string expected = Strings.Exceeded;

            Assert.AreEqual(expected, ret);

        }

        [TestMethod]
        public void GetUsageMessageTest_One()
        {
            string ret = new Authentication().GetUsageMessage(1);
            string expected = Strings.OneMoreRight;

            Assert.AreEqual(expected, ret);

        }

        [TestMethod]
        public void GetUsageMessageTest_OK()
        {
            int usageRight = 3;
            string ret = new Authentication().GetUsageMessage(usageRight);
            string expected = $"You may ask for {usageRight} times more.";

            Assert.AreEqual(expected, ret);

        }

        [TestMethod]
        public void LastCharacterAsByteTest_9()
        {
            byte ret = "test9".LastCharacter().AsByte();
            Assert.AreEqual(9,ret);
        }

        [TestMethod]
        public void LastCharacterAsByteTest_string()
        {
            byte ret = "test".LastCharacter().AsByte();
            Assert.AreEqual(0, ret);
        }

        [TestMethod]
        public void LastCharacter_9()
        {
            string ret = "test9".LastCharacter();
            Assert.AreEqual("9", ret);

        }

        [TestMethod]
        public void LastCharacter()
        {
            string ret = "test0".LastCharacter();
            Assert.AreEqual("0", ret);

        }

        


    }
}
